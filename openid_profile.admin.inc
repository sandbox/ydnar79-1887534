<?php
/**
 * @file
 *
 */

/**
 * Form of the mapping, map CCK fields to ax schema attributes
 */
function openid_profile_map($form, $form_state) {
  $mapping = $attributes = $mapped_attributes = $unmapped_attributes = array();
  $source = openid_profile_get_mapping($mapping);
  $already_mapped = array_keys($mapping);

  ctools_include('plugins');
  $plugins = ctools_get_plugins('openid_profile', 'field_ax');

  // Get supported attributes and fields from plugins
  foreach ($plugins as $name => $plugin) {
    $plugin = openid_profile_load_plugin($plugin['name']);
    if (method_exists($plugin, 'get_attributes')) {
      $attributes += (array) $plugin->get_attributes();
    }
  }

  // Allow other modules to hook into the profile field definition
  drupal_alter('openid_profile', $attributes);

  // Determin mapped attributes
  foreach ($mapping as $mapped_field) {
    $mapped_attributes[$mapped_field['value']] = $attributes[$mapped_field['value']];
  }
  // Determin unmapped attributes
  $unmapped_attributes = array_diff($attributes, $mapped_attributes);

  $options_openid = array();
  $openid_attrs = openid_ax_api_schema_definitions();

  if (module_exists('openid_provider_ax') || module_exists('openid_client_ax')) {
    foreach ($openid_attrs as $id => $attr) {
      if (!in_array($id, $already_mapped)) {
        $options_openid += array($id => 'AX: ' . $attr['label']);
      }
    }
  }

  $openid_sreg_fields = array();
  if (module_exists('openid_provider_sreg') || module_exists('openid_client_sreg')) {
    foreach ($openid_attrs as $id => $attr) {
      if (isset($attr['sreg']) && !in_array($attr['sreg'], $already_mapped)) {
        $openid_sreg_fields += array($attr['sreg'] => 'SReg: ' . $attr['label']);
      }
    }
  }

  if (!module_exists('openid_client_ax') && !module_exists('openid_provider')) {
    $form['warning']['#value'] = t('Enable OpenID Client AX module to use this module on an OpenID client site.');
    return $form;
  }
  elseif (!(module_exists('openid_provider_sreg') || module_exists('openid_provider_ax')) && !module_exists('openid_client_ax')) {
    $form['warning']['#value'] = t('Enable OpenID Provider SReg, OpenID Provider AX or both to use the module on an OpenID provider site.');
    return $form;
  }

  // Collect sreg labels
  $sreg_fields = array();
  foreach ($openid_attrs as $id => $attr) {
    if (isset($attr['sreg'])) {
      $sreg_fields[$attr['sreg']] = $attr['label'];
    }
  }

  // Generate table for showing the current mapping
  $rows = array();
  foreach ($mapping as $openid => $field) {
    if (isset($field['handler']['name'])) {
      $handler = '<i>' . $field['handler']['name']. '</i>';
    }
    else {
      $handler = '<i><strong>' . t('missing') . '</i></strong>';
    }

    if (!strstr($openid, '.sreg.')) {
      $rows[] = array(
        'node' => $attributes[$field['value']],
        'openid' => 'AX: ' . $openid_attrs[$openid]['label'],
        'handler' => '<i>' . $handler . '</i>',
        'link' => l(t('Delete'), "openid_profile/delete/" . base64_encode($openid)),
      );
    }
    else {
      $rows[] = array(
        'node' => $attributes[$field_name],
        'openid' => 'SReg: ' . $sreg_fields[$openid],
        'handler' => '<i>' . $handler . '</i>',
        'link' => l(t('Delete'), "openid_profile/delete/" . base64_encode($openid) . '/sreg'),
      );
    }
  }

  $current_map = theme('table', array(
    'header' => array(
      'node' => t('Field'),
      'openid' => t('OpenID field'),
      'handler' => t('Handler'),
      'link' => t('Operation')),
    'rows' => $rows,
    'empty' => t('No mapping defined.')));

  if ($source == OPENID_PROFILE_DEFAULT) {
    $status = t('(default)');
  }
  elseif ($source == OPENID_PROFILE_OVERRIDDEN) {
    $status = t('(default, <em>overridden</em>)');
  }
  else { // OPENID_PROFILE_NORMAL
    $status = '';
  }
  $form['current_map'] = array(
    '#type' => 'fieldset',
    '#title' => t('Mappings !status', array('!status' => $status)),
    '#collapsible' => FALSE,
  );
  $form['current_map']['map'] = array(
    '#markup' => isset($current_map) ? $current_map : '',
  );
  if ($source == OPENID_PROFILE_OVERRIDDEN) {
    $form['current_map']['revert'] = array(
      '#type' => 'submit',
      '#value' => t('Revert'),
      '#submit' => array('openid_profile_map_revert_submit'),
    );
  }
  $form['map'] = array(
    '#type' => 'fieldset',
    '#title' => t('Map user fields to OpenID fields'),
  );
  if (count($unmapped_attributes) == 0 || (count($openid_attrs) == 0 && count($openid_sreg_fields) == 0)) {
    $form['map']['info'] = array('#markup' => t('No more fields to map.'));
  }
  else {
    $form['#attached']['css'] = array(drupal_get_path('module', 'openid_profile') . "/openid_profile.css");
    $form['map']['map_a'] = array(
      '#type' => 'select',
      '#title' => t('User fields'),
      '#options' => $unmapped_attributes,
    );
    $form['map']['map_b'] = array(
      '#type' => 'select',
      '#title' => t('OpenID field'),
      '#options' => $options_openid + $openid_sreg_fields,
    );
    $form['map']['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Add'),
      '#submit' => array('openid_profile_map_add_submit'),
    );
    $form['map']['#description'] = t('Fields defined by the <a href="http://openid.net/specs/openid-attribute-exchange-1_0.html">Attribute Exchange</a> specification are prefixed AX, fields defined by the <a href="http://openid.net/specs/openid-simple-registration-extension-1_0.html">Simple Registration</a> specification are prefixed SReg.');
  }

  // Define read-only fields on the client
  if (module_exists('openid_client_ax')) {
    $form['readonly'] = array(
      '#type' => 'fieldset',
      '#title' => t('Define read-only fields.'),
      '#description' => t('Select the fields that can only be changed on the provider side.'),
    );
    $form['readonly']['openid_profile_provider_account_link'] = array(
      '#type' => 'checkboxes',
      '#title' => t('Readonly fields.'),
      '#options' => _openid_profile_read_only_prepare($attributes),
      '#default_value' => variable_get('openid_profile_read_only_fields', array()),
    );
    $form['readonly']['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Save'),
      '#submit' => array('openid_profile_read_only_submit'),
    );
  }

    return $form;
}

function openid_profile_read_only_submit($form, &$form_state) {
  variable_set('openid_profile_provider_account_link', $form_state['values']['openid_profile_provider_account_link']);
}

/**
 * Saves the user-defined mapping.
 */
function openid_profile_map_add_submit($form, &$form_state) {
  $mapping = array();
  openid_profile_get_mapping($mapping);

  if (!empty($form_state['values']['map_b']) && !empty($form_state['values']['map_a'])) {

    // Getting field type or name of field in case of immediate user's fields
    $field_elements = explode('::', $form_state['values']['map_a']);
    $field_name = array_shift($field_elements);
    if (!$field_info = field_info_field($field_name)) {
      $field_info = array('type' => $field_name);
    }

    // Selecting adeqaute handler for field type to AX conversion
    ctools_include('plugins');
    $plugins = ctools_get_plugins('openid_profile', 'field_ax');

    $valid_plugins = array();
    foreach ($plugins as $plugin) {
      if (in_array($field_info['type'], $plugin['field_types'])) {
        $valid_plugins[] = $plugin;
      }
    }

    // @TODO: Move this check to validation hook
    if (!empty($valid_plugins)) {
      $mapping += array(
        $form_state['values']['map_b'] => array(
          'value'=> $form_state['values']['map_a'],
          // @TODO: Make handlers selectable through the UI
          'handler' => $valid_plugins[0]['handler'],
          'field_type' => $field_info['type'],
        ),
      );
      variable_set('openid_profile_map', $mapping);
    }
    else {
      drupal_set_message(t('No valid AX convertion handler found for the selected field'), 'error');
    }
  }
}

function openid_profile_map_revert_submit($form, &$form_state) {
  $mapping_imported = module_invoke_all('openid_profile_mapping');
  variable_set('openid_profile_map', $mapping_imported[$type]);
}

/**
 * Shows the current mapping code to the user.
 */
function openid_profile_export($form, $form_state, $type = '') {
  $mapping = array();
  openid_profile_get_mapping($mapping, $type);
  $form['type'] = array(
    '#type' => 'value',
    '#value' => $type,
  );
  $array = '$mapping = array();';
  $head = "\$mapping[\"$type\"] = ";
  $tail = ';';
  $return = '
return $mapping;';
  $form['show'] = array(
    '#title' => t('PHP code'),
    '#type' => 'textarea',
    '#default_value' => $array . $head . var_export($mapping, TRUE) . $tail . $return,
    '#description' => t('This code can be pasted into an implementation of hook_openid_profile_mapping().'),
  );
  return $form;
}

/**
 * Deletes the user-defined mapping
 */
function openid_profile_delete() {
  $openid = base64_decode(arg(2));
  $mapping = array();
  $source = openid_profile_get_mapping($mapping);
  if ($source == OPENID_PROFILE_DEFAULT) {
    variable_set('openid_profile_map', $mapping);
  }
  $mapping = variable_get('openid_profile_map', array());
  unset($mapping[$openid]);
  variable_set('openid_profile_map', $mapping);
  drupal_goto('admin/config/people/accounts/openid_ax_mapping/edit');
}

function _openid_profile_read_only_prepare($attributes) {
  $read_only = array();
  foreach ($attributes as $name => $label) {
    $field_elements = explode('::', $name);
    $field_name = array_shift($field_elements);
    if ($field_instance = field_info_instance('user', $field_name, 'user')) {
      $read_only[$field_name] = t('Field: @label', array('@label' => $field_instance['label']));
    }
    else {
      $read_only[$name] = $label;
    }
  }
  return $read_only;
}

/**
 * Gives additional options for modifying the user experience.
 */

function openid_profile_additional_client_options($form, $form_state) {

    // Define additional admin options to improve user interface.
    $form['description'] = array(
      '#markup' => t('<div>Below you will find additonal options that allow you to customize the user experience with OpenID Profile.</div>'),
    );

/*    $form['network_name'] = array(
      '#type' => 'fieldset',
      //'#title' => t('Provider Name.'),
      //'#description' => t('Below you will find additonal options that allow you to customize the user experience with OpenID Profile.'),
    );
    $form['network_name']['openid_profile_sso_provider_name'] = array(
      '#type' => 'textfield',
      '#title' => t('Single Sign On Provider Name'),
      '#description' => "Set the name of the site that you are using as the Single Sign On Provider or leave it generic.",
      '#default_value' => variable_get('openid_profile_sso_provider_name', "Single-Sign-On-Provider"),
      '#required' => TRUE,
);*/
    $form['sync_options'] = array(
      '#type' => 'fieldset',
      '#title' => t('Synchronization Options'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#description' => t('Choose what should be schronized and when.  These settings effect both directions of
        synchronization between the Identity Provider and the Relying Party.'),
    );
    $form['sync_options']['login_options'] = array(
      '#type' => 'fieldset',
      '#title' => t('Login Options'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#description' => t('This area allows you to adjust how data is imported from the Identity Provider on login.'),
    );
    $form['sync_options']['login_options']['openid_profile_update_on_login'] = array(
      '#type' => 'radios',
      '#title' => t('Update on Login Setting'),
      '#default_value' => variable_get('openid_profile_update_on_login', "update_on_login"),
      '#options' => array(
            'update_on_login' => t('On Login, update the local user account from the Identity Provider.  This is the original default.'),
            'ask_update_on_login' => t('The user will choose if they want to update the local user account from the Identity Provider.'),
            'no_update_on_login' => t('On Login, DO NOT update the local account from the Identity Provider.  DO NOT give the user a choice.'),
       ),
      '#description' => "How do you want this site to act upon user login.  Should it update the user account or not?
        Or should it be left up to the user on an individual basis?
        <br> NOTE: This has no effect on READ ONLY fields.  They WILL be updated on each login.",
      '#required' => TRUE,
);
    $form['sync_options']['login_options']['openid_profile_update_on_login_user_default'] = array(
      '#type' => 'radios',
      '#title' => t('User Default for Update on Login Setting'),
      '#default_value' => variable_get('openid_profile_update_on_login_user_default', "update_on_login"),
      '#options' => array(
            'update_on_login' => t('On Login, update the local user account from the Identity Provider.  This is the original default.'),
            'no_update_on_login' => t('On Login, DO NOT update the local account from the Identity Provider.'),
       ),
      '#description' => "If you allow the user to choose to update on login or not.  Which choice should be DEFAULT.
        <br> NOTE:  This has no effect unless the user is given the choice above.",
      '#required' => TRUE,
      '#states' => array(
          // Show only when needed.
          'visible' => array(
          ':input[name="openid_profile_update_on_login"]' => array('value' => 'ask_update_on_login'),
          ),
        ),
);
    $form['sync_options']['account_update_options'] = array(
      '#type' => 'fieldset',
      '#title' => t('Account Edit Options'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#description' => t('Choose what happens when an account is edited.  Does the Identity Provider get updated too?'),
    );
    $form['sync_options']['account_update_options']['openid_profile_update_on_account_update'] = array(
      '#type' => 'radios',
      '#title' => t('Update on Account Edit'),
      '#default_value' => variable_get('openid_profile_update_on_account_update', "update_on_account_edit"),
      '#options' => array(
            'update_on_account_edit' => t('On Account Edit, update Identity Provider.  This is the original default.'),
            'ask_update_on_account_edit' => t('The user will choose if they want to update the profile on the Identity Provider.'),
            'no_update_on_account_edit' => t('On Account Edit, DO NOT update profile on the Identity Provider.  DO NOT give the user a choice.'),
       ),
      '#description' => "When a local account is edited, should the Identity Provider be updated also?  Or leave
        it up to the user?  <br> NOTE: This has no effect on READ ONLY fields.  They can ONLY be updated on the
        Identity Provider website.",
      '#required' => TRUE,
    );


    $form['sync_options']['account_update_options']['openid_profile_update_on_account_update_user_default'] = array(
      '#type' => 'radios',
      '#title' => t('User Default for Update on Account Edit'),
      '#default_value' => variable_get('openid_profile_update_on_account_update_user_default', "update_on_account_edit"),
      '#options' => array(
            'update_on_account_edit' => t('On Account Edit, update Identity Provider.  This is the original default.'),
            'no_update_on_account_edit' => t('On Account Edit, DO NOT update profile on the Identity Provider.'),
       ),
      '#description' => "If you allow the user to choose to update the Identity Provider on local account edits.
        Which choice should be DEFAULT.
        <br> NOTE:  This has no effect unless the user is given the choice above.",
      '#required' => TRUE,
      '#states' => array(
          // Show only when needed.
          'visible' => array(
          ':input[name="openid_profile_update_on_account_update"]' => array('value' => 'ask_update_on_account_edit'),
          ),
        ),
    );

    $form['provider_account_link'] = array(
      '#type' => 'fieldset',
      '#title' => t('Identity Provider Links'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#description' => t('How do you want the links to the Identity Provider on the account edit page to work?'),
    );

    $form['provider_account_link']['openid_profile_provider_link_method'] = array(
      '#type' => 'radios',
      '#title' => t('Type of Link'),
      '#default_value' => variable_get('openid_profile_provider_link_method', "default_link"),
      '#options' => array(
            'default_link' => t('REDIRECT - Links on the account edit page will redirect to OpenID SSO Provider site.  This is the original default.'),
            'new_window' => t('NEW WINDOW - Links will open in a new (_blank) window.'),
            'no_link' => t('NO LINK - This will output plain text only, without any form of link.  THIS IS NOT RECOMMENDED!'),
       ),
      '#description' => "NOTE: By installing the COLORBOX module you will have additional options available.",
      '#required' => TRUE,
);


  //  If the colorbox module is installed, give additional options for the configuration.
  if (module_exists('colorbox')) {

    $form['provider_account_link']['openid_profile_provider_link_method']['#options']['colorbox_redirect'] = t('COLORBOX / REDIRECT -
      The links will open in a colorbox popup.
      <br> This will fall back to redirecting the user to the OpenID SSO Provider if javascript is disabled.');

    $form['provider_account_link']['openid_profile_provider_link_method']['#options']['colorbox_new_window'] = t('COLORBOX / NEW WINDOW -
      The links will open in a colorbox popup.
      <br> This will fall back to opening a new window directing the user to the OpenID SSO Provider if javascript is disabled.');

    $form['provider_account_link']['openid_profile_provider_link_destination'] = array(
      '#type' => 'radios',
      '#title' => t('Colorbox Destination'),
      '#default_value' => variable_get('openid_profile_provider_link_destination', "full_profile"),
      '#options' => array(
            'full_profile' => t('MAIN PROFILE - The user will be sent to their account view page.'),
            'edit_only' => t('EDIT PROFILE ONLY - The user will be sent straight to their account edit page.'),
       ),
      '#description' => "When the user is sent to their MAIN PROFILE, a theme that allows for TABS will be necessary.
        However, if the EDIT PROFILE ONLY option is chosen, it is recommended to choose a theme WITHOUT TABS so that
        the user cannot navigate away from that page.",
      '#required' => TRUE,
      '#states' => array(
          // Hide the settings when they are not necessary.
          // The "option1", "option2" and the "!value" below are necessary to use a trick to create an OR check instead of and AND.
          'visible' => array(
          ':input[name="openid_profile_provider_link_method"], option1' => array('!value' => 'default_link'),
          ':input[name="openid_profile_provider_link_method"], option2' => array('!value' => 'new_window'),
          ':input[name="openid_profile_provider_link_method"], option3' => array('!value' => 'no_link'),
        ),
    ),

);

    $form['provider_account_link']['advanced_themeing'] = array(
      '#type' => 'fieldset',
      '#title' => t('Advanced Themeing'),
      '#description' => t("The below theme(s) are completely OPTIONAL.  If set, it will OVERRIDE the default
        theme settings in the OpenID Profile configuration on the Identity Provider.  This is useful for custom themeing on a site by site basis.
        <br>  VERY IMPORTANT: When set, the themes MUST be installed on the provider site in order to for this to work correctly."),
      '#states' => array(
          // Hide the settings when they are not necessary.
          // The "option1", "option2" and the "!value" below are necessary to use a trick to create an OR check instead of and AND.
          'visible' => array(
          ':input[name="openid_profile_provider_link_method"], option1' => array('!value' => 'default_link'),
          ':input[name="openid_profile_provider_link_method"], option2' => array('!value' => 'new_window'),
          ':input[name="openid_profile_provider_link_method"], option3' => array('!value' => 'no_link'),
        ),
    ),
    );

    $form['provider_account_link']['advanced_themeing']['openid_profile_full_profile_theme_name'] = array(
      '#type' => 'textfield',
      '#title' => t('MAIN PROFILE - Colorbox Theme (Optional)'),
      '#default_value' => variable_get('openid_profile_full_profile_theme_name'),
      '#description' => "Enter in the MACHINE NAME of the theme that you want to use for pages displayed inside of the colorbox.
        <br>  NOTE: Ordinarily, this theme should only consist of the page title, a message area, tabs menu and the main content area.
        This is done to only show what is useful to the user in a popup. (No sidebars, main menus, etc.)",
      '#states' => array(
          'visible' => array(
          ':input[name="openid_profile_provider_link_destination"], option1' => array('value' => 'full_profile'),
        ),
    ),
   );
    $form['provider_account_link']['advanced_themeing']['openid_profile_full_profile_blank_theme_name'] = array(
      '#type' => 'textfield',
      '#title' => t('MAIN PROFILE - Optional BLANK Theme'),
      '#default_value' => variable_get('openid_profile_full_profile_blank_theme_name'),
      '#description' => "Enter in the MACHINE NAME of the theme that you want to use for pages displayed inside of the colorbox when NO TABS should be seen.
        This can be the same theme that you would use for the EDIT PROFILE ONLY links if you were using them.
        <br> This is needed if the user is NOT logged in and is redirected to a login screen.  Therefore, there should be NO
        TABS visible in the theme.  If NOT set, the Identity Provider website will use the default theme.
        <br>  NOTE: Ordinarily, this theme should only consist of the page title, a message area and the main content area.
        This is done to only show what is useful to the user in a popup. (No sidebars, menus, etc.)",
      '#states' => array(
          'visible' => array(
          ':input[name="openid_profile_provider_link_destination"], option1' => array('value' => 'full_profile'),
        ),
    ),
   );

    $form['provider_account_link']['advanced_themeing']['openid_profile_edit_profile_only_theme_name'] = array(
      '#type' => 'textfield',
      '#title' => t('EDIT PROFILE ONLY - Colorbox Theme (Optional)'),
      '#default_value' => variable_get('openid_profile_edit_profile_only_theme_name'),
      '#description' => "Enter in the MACHINE NAME of the theme that you want to use for pages displayed inside of the colorbox.
        <br>  NOTE: Ordinarily, this theme should only consist of the page title, a message area and the main content area.
        This is done to only show what is useful to the user in a popup. (No sidebars, menus, etc.)",
      '#states' => array(
          'visible' => array(
          ':input[name="openid_profile_provider_link_destination"], option1' => array('value' => 'edit_only'),
        ),
    ),
   );

  }
    $form['reset'] = array(
      '#value' => t('Reset'),
      '#type' => 'submit',
      '#submit' => array('openid_profile_additional_client_options_reset'),
    );

  return system_settings_form($form);
}

function openid_profile_additional_client_options_reset($form, &$form_state) {
  variable_del('openid_profile_update_on_login');
  variable_del('openid_profile_update_on_account_update');
  variable_del('openid_profile_update_on_login_user_default');
  variable_del('openid_profile_update_on_account_update_user_default');
  variable_del('openid_profile_provider_link_method');
  variable_del('openid_profile_provider_link_destination');
  variable_del('openid_profile_full_profile_theme_name');
  variable_del('openid_profile_full_profile_blank_theme_name');
  variable_del('openid_profile_edit_profile_only_theme_name');

  drupal_set_message(t('Settings reset.'));

}


/**
 * Gives additional options for modifying the user experience.
 */

function openid_profile_additional_provider_options($form, $form_state) {

    // Define additional admin options to improve user interface.
    $form['description'] = array(
      '#markup' => t('<div>Below you will find additonal options that allow you to customize the user experience with OpenID Profile.</div>'),
    );

/*    $form['network_name'] = array(
      '#type' => 'fieldset',
      //'#title' => t('Provider Name.'),
      //'#description' => t('Below you will find additonal options that allow you to customize the user experience with OpenID Profile.'),
    );
    $form['network_name']['openid_profile_sso_provider_name'] = array(
      '#type' => 'textfield',
      '#title' => t('Single Sign On Provider Name'),
      '#description' => "Set the name of the site that you are using as the Single Sign On Provider or leave it generic.",
      '#default_value' => variable_get('openid_profile_sso_provider_name', "Single-Sign-On-Provider"),
      '#required' => TRUE,
);*/
    $form['themeing'] = array(
      '#type' => 'fieldset',
      '#title' => t('Themeing Options'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#description' => t("Below you will find themeing options that will effect the appearance for your users when using
        the colorbox popups on the OpenID Relying Party websites when clicking on the links found on their account edit page.
        These are the DEFAULT themes used
        in the colorbox popups.  However, they CAN be OVERRIDEN by using custom theme settings on the individual
        OpenID Relying Party websites.  (Allowing additional themeing per site, if neccessary.)
        <br> VERY IMPORTANT: The themes MUST be installed on THIS site for this to work correctly."),
    );
    $form['themeing']['openid_profile_provider_full_profile_theme_name'] = array(
      '#type' => 'textfield',
      '#title' => t('MAIN PROFILE - Colorbox Theme'),
      '#default_value' => variable_get('openid_profile_provider_full_profile_theme_name', 'blank_with_title_tabs_messages'),
      '#required' => TRUE,
      '#description' => "Enter in the MACHINE NAME of the theme that you want to use for pages displayed inside of the colorbox.
        <br>  NOTE: Ordinarily, this theme should only consist of the page title, a message area, tabs menu and the main content area.
        This is done to only show what is useful to the user in a popup. (No sidebars, main menus, etc.)",
   );
    $form['themeing']['openid_profile_provider_full_profile_blank_theme_name'] = array(
      '#type' => 'textfield',
      '#title' => t('MAIN PROFILE - Colorbox NO TABS Theme'),
      '#default_value' => variable_get('openid_profile_provider_full_profile_blank_theme_name', 'blank_with_title_and_messages'),
      '#description' => "Enter in the MACHINE NAME of the theme that you want to use for pages displayed inside of the colorbox when NO TABS should be seen.
        This can be the same theme that you would use for the EDIT PROFILE ONLY area.
        <br> This is needed if the user is NOT logged in and is redirected to a login screen.  Therefore, there should be NO
        TABS visible in the theme.
        <br>  NOTE: Ordinarily, this theme should only consist of the page title, a message area and the main content area.
        This is done to only show what is useful to the user in a popup. (No sidebars, menus, etc.)",
   );

    $form['themeing']['openid_profile_provider_edit_profile_only_theme_name'] = array(
      '#type' => 'textfield',
      '#title' => t('EDIT PROFILE ONLY - Colorbox Theme'),
      '#default_value' => variable_get('openid_profile_provider_edit_profile_only_theme_name', 'blank_with_title_and_messages'),
      '#required' => TRUE,
      '#description' => "Enter in the MACHINE NAME of the theme that you want to use for pages displayed inside of the colorbox.
        <br>  NOTE: Ordinarily, this theme should only consist of the page title, a message area and the main content area.
        This is done to only show what is useful to the user in a popup. (No sidebars, menus, etc.)",
   );

    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Save configuration'),
      '#submit' => array('system_settings_form_submit'),
    );
    $form['reset'] = array(
      '#value' => t('Reset'),
      '#type' => 'submit',
      '#submit' => array('openid_profile_additional_provider_options_reset'),
    );

    return $form;
}

function openid_profile_additional_provider_options_reset($form, &$form_state) {
  variable_del('openid_profile_provider_full_profile_theme_name');
  variable_del('openid_profile_provider_full_profile_blank_theme_name');
  variable_del('openid_profile_provider_edit_profile_only_theme_name');

  drupal_set_message(t('Settings reset.'));

}

